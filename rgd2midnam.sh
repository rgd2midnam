#!/bin/sh
test $# != 1 && echo "Usage: $0 <rgd-file>" && exit 1

echo '<?xml version="1.0" encoding="UTF-8"?>'
echo '<!DOCTYPE MIDINameDocument PUBLIC "-//MIDI Manufacturers Association//DTD MIDINameDocument 1.0//EN" "http://www.midi.org/dtds/MIDINameDocument10.dtd">'
echo
gunzip -c $1 | xsltproc --param filename \"$(basename $1 .lscp.rgd)\" rgd2midnam.xsl - | xmllint --format --encode UTF-8 --dtdvalid http://www.midi.org/dtds/MIDINameDocument10.dtd - | sed '1d'
